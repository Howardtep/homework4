import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: Scaffold(
        body: Center(
          child: LogoApp(),
        ),
      ),
    );
  }
}
class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> rotation;
  Animation<double> travel;
  AnimationController controller;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      duration: Duration(seconds: 5),
      vsync: this,
    );

    rotation = Tween<double>(begin: 0, end: 5,).animate(controller);
    travel = Tween<double>(begin: 0, end: 300,).animate(controller);
    //travel = Tween<double>(begin:300, end: 0,).animate(controller);
    controller.repeat();

  }

    @override
    Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (_, child) => Stack(children: <Widget>[
        Positioned(
          top: 100,
          left: travel.value,
          child: Transform(
            transform: Matrix4.rotationZ(rotation.value),
            alignment: Alignment.center,
            child: Image.asset('images/Star.png'),
          ),
        ),
        Positioned(
          top: 300,
          left: travel.value,
          child: Transform(
            transform: Matrix4.rotationZ(rotation.value),
            alignment: Alignment.center,
            child: Image.asset('images/Star.png'),
          ),
        ),
        Positioned(
          top: 500,
          left: travel.value,
          child: Transform(
            transform: Matrix4.rotationZ(rotation.value),
            alignment: Alignment.center,
            child: Image.asset('images/Star.png'),
          ),
        ),
      ])
    );
    }

    @override
    void dispose() {
      controller.dispose();
      super.dispose();
    }

}